import VideoListItem from "../components/VideoListItem";

function VideoList(props){

    function receiveCallback(movie){
        props.callbackapp(movie)
    }
    return(
        <>
        <ul>
            {props.moviesList.map(movie => {
                return <VideoListItem key={movie.id} movie={movie} callback={receiveCallback}/>
            })}
        </ul>
        </>
    )
}
export default VideoList;