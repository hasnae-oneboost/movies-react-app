import React from 'react';
import SearchBar from '../components/SearchBar';
import VideoList from './VideoList';
import axios from "axios";
import VideoDetail from '../components/VideoDetail';
import Video from '../components/Video';
import "../style/style.css"
//import { Col } from 'react-bootstrap';

const API_END_POINT = "https://api.themoviedb.org/3/"
const POPULAR_MOVIES_URL = "discover/movie?language=en&sort_by=popularity.desc&include_adult=false&append_to_response=images"
const API_KEY = "api_key=db9e79d78acb47ae701a35955948a971"
const SEARCH_URL = "search/movie?&language=en-US&page=1&include_adult=false"



class App extends React.Component{
    constructor(props){
        super(props)
        this.state={moviesList:{},currentMovie:{}}
    }

    componentWillMount(){
       this.initMovies();
    }

    //Liste des films populaires + Film courrant
    initMovies(){
        axios.get(`${API_END_POINT}${POPULAR_MOVIES_URL}&${API_KEY}`).then(function(response){
            this.setState({moviesList:response.data.results.slice(1,7),currentMovie:response.data.results[0]},function(){
                this.videoCurrentMovie();
                
            })
        }.bind(this))
    }   

    //Youtube video pour Current movie
    videoCurrentMovie(){
        axios.get(`${API_END_POINT}movie/${this.state.currentMovie.id}/videos?&${API_KEY}&language=en-US`).then(function(response){
            if(response.data.results.length){            
                const youtubeKey =response.data.results[0].key;
                let newCurrentMovie = this.state.currentMovie;
                //nouveau champ videoId contient youtube key
                newCurrentMovie.videoId = youtubeKey;
                this.setState({currentMovie: newCurrentMovie});               
            }
            }.bind(this));
        
    }

    onClickListItem(movie){
        this.setState({currentMovie:movie}, function(){
            this.videoCurrentMovie();
            this.setRecommendation();
        });
    }

    //Recommendation list
    setRecommendation(){
        axios.get(`${API_END_POINT}movie/${this.state.currentMovie.id}/recommendations?${API_KEY}&language=en-US&page=1`).then(function(response){
            this.setState({moviesList:response.data.results.slice(0,6)})
        }.bind(this))
    } 

    noSearchResult(){
        console.log("no data")
    }

    onClickSearch(search){
        if(search){
            axios.get(`${API_END_POINT}${SEARCH_URL}&${API_KEY}&query=${search}`).then(function(response){
               if(response.data && response.data.results[0]){
                    if(response.data.results[0].id !== this.state.currentMovie.id){
                        this.setState({currentMovie: response.data.results[0]}, ()=>{
                            this.videoCurrentMovie();
                            this.setRecommendation();
                        })
                    }
                }
                else return this.noSearchResult();       
            }.bind(this));

        }
        
    }
    render(){
        const renderVideoList = () => {
            if(this.state.moviesList.length>=5){
                return <VideoList moviesList={this.state.moviesList} callbackapp={this.onClickListItem.bind(this)}/>   

            }
        }
        return(
            <div className="container">
                <div className="search_bar">
                    <SearchBar callbackSearch={this.onClickSearch.bind(this)} />
                </div>
                <div className="row">
                    <div className="col-md-8">
                        <Video videoId={this.state.currentMovie.videoId} />
                        <VideoDetail title={this.state.currentMovie.title} overview={this.state.currentMovie.overview}/>
                    </div>
                    <div className="col-md-4">
                        {renderVideoList()}
                    </div>
                
                </div>

            </div>
        )
    }
}
export default App;