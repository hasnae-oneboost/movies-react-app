import VideoNotFound from "./VideoNotFound";

const BASE_URL="https://www.youtube.com/embed/";

function Video({videoId}){

    if(videoId){  
        return(
        <>
        <div className="embed-responsive embed-responsive-16by9">
            <iframe className="embed-responsive-item" title="movie trailer" src={`${BASE_URL}${videoId}`} />
        </div>
        </>
    )}

      else return <VideoNotFound/>
    
    
    
  
}
export default Video; 