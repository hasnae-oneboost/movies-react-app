
function VideoDetail({title,overview}){
    return(
        <>
            <h3>{title}</h3>
            <p>{overview}</p>

        </>
    )
}
export default VideoDetail;