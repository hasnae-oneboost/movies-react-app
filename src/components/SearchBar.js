import React from "react";

class SearchBar extends React.Component{
    constructor(props){
        super(props);
        this.state={
            searchText:"",
            intervalBeforeRequest:1000,
            lockRequest: false
        }
    }

    handleChange(e){
      this.setState({searchText:e.target.value})
      if(!this.state.lockRequest){
          this.setState({lockRequest:true})
          setTimeout(function(){this.search()}.bind(this),this.state.intervalBeforeRequest)
      }
    }

    handleClick(){
        this.search();
    }

    search(){
       this.props.callbackSearch(this.state.searchText) ;
       this.setState({lockRequest:false})
    }

    render(){
        return(
            <div className="row">
                <div className="col-md-8 input-group"> 
                    <input placeholder="Search" type="text" className="form-control input-lg" onChange={this.handleChange.bind(this)} />
                    <span className="input-group-btn">
                        <button className="btn btn-outline-info" onClick={this.handleClick.bind(this)}>GO!</button>
                    </span>
                </div>
            </div>
        )
    }    
}

export default SearchBar;