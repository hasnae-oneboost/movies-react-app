const IMAGE_BASE_URL ="https://image.tmdb.org/t/p/w500/";

function VideoListItem(props){


    function handleOnClick(){
        props.callback(props.movie);
    }

    return(
        <>
        <li className="list-group-item" onClick={handleOnClick}>
            <div className="media">
                <div className="media-left">
                    <img className="media-object img-rounded" src={`${IMAGE_BASE_URL}${props.movie.poster_path}`} alt="movie_poster" width="80px"/>
                </div>
                <div className="media-body">
                    <p className="title_list_item">{props.movie.title}</p>
                </div>
            </div>

        </li>
        </>
    )


}
export default VideoListItem;